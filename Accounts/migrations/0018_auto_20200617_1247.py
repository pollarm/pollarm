# Generated by Django 3.0.5 on 2020-06-17 07:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Accounts', '0017_auto_20200615_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='poll',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Accounts.Poll'),
        ),
        migrations.AlterField(
            model_name='poll',
            name='room',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='Accounts.poll_room'),
        ),
        migrations.AlterField(
            model_name='vote',
            name='participant',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='Participant', to='Accounts.Participents'),
        ),
    ]
