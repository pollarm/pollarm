# Generated by Django 3.0.5 on 2020-06-18 07:02

from django.db import migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('Accounts', '0021_pollcompleted_room_data'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pollcompleted',
            old_name='room_data',
            new_name='choice_data',
        ),
        migrations.AddField(
            model_name='pollcompleted',
            name='poll_data',
            field=picklefield.fields.PickledObjectField(default=0, editable=False),
            preserve_default=False,
        ),
    ]
