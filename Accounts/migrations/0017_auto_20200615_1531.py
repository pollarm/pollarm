# Generated by Django 3.0.5 on 2020-06-15 10:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Accounts', '0016_auto_20200615_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='participents',
            name='choice_select',
        ),
        migrations.AddField(
            model_name='vote',
            name='participant',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='Participant', to='Accounts.Participents'),
            preserve_default=False,
        ),
    ]
