# Generated by Django 3.0.5 on 2020-06-21 16:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Accounts', '0025_auto_20200621_2114'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pollvotesrecord',
            name='vote',
        ),
        migrations.AddField(
            model_name='pollcompleted',
            name='participants',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pollvotesrecord',
            name='question',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='Questions', to='Accounts.Poll'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='pollvotesrecord',
            name='choice',
            field=models.TextField(),
        ),
    ]
