import json
import random



from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.db.models import Count
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, Http404, get_object_or_404
from django.contrib.auth.models import User,auth
from django.contrib import auth
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q


from ARM import settings
from .models import  Choice, Poll, Vote,poll_room,Participents,Categories,PollCompleted,QuestionsRecord
from .forms import PollAddForm,EditPollForm,ChoiceAddForm,PollRoomForm
from django.http import HttpResponseRedirect
import csv


def export_record_xls(request, id):
    questions_list=[]
    queryset = PollCompleted.objects.get(id=id)
    poll = Poll.objects.filter(room=queryset.room)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Poll Record.csv"'
    writer = csv.writer(response)
    writer.writerow(['Poll Title: '+str(queryset.room.title) , 'Poll Description: '+str(queryset.room.dscp), 'Poll Completed on: '+ str(queryset.completed_date), 'Total Participants: '+ str(queryset.participants), 'Poll Hosted with Code: '+str(queryset.code_room)])
    writer.writerow(['Questions', 'Choices and Votes',])
    # instance = queryset
    c = queryset.poll_data
    for d in c['users']:
        questions_list.append(d['id'])
        print(questions_list)
    p = Poll.objects.filter(id__in=questions_list)
    ques = QuestionsRecord.objects.filter(recorded_date=queryset)
    print(ques)
    for question in ques:
        writer.writerow([question.poll])
        for stock in question.vote_data:

            writer.writerow([stock['text'], stock['num_votes'],str(stock['percentage'])+"%",  ])
    return response


def export_users_xls(request,id):
        date_list=[]
        particpants = []
        queryset = poll_room.objects.get(id=id)
        c = PollCompleted.objects.filter(room=queryset)
        for s in c:
            date_list.append(s)
        poll = Poll.objects.filter(room=queryset)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Latest Poll Record.csv"'
        writer = csv.writer(response)
        writer.writerow(['Poll Title: ' + str(queryset.title), 'Poll Description: ' + str(queryset.dscp),

                        ])
        writer.writerow(['Questions', 'Choices and Votes', 'Percentage'])
        # instance = queryset
        for question in poll:
            writer.writerow(["QUESTION: "+str(question)])
            for stock in question.get_result_dict():
                writer.writerow([stock['text'], stock['num_votes'], str(stock['percentage']) + "%", ])

        writer.writerow(["   "+"Choices answered by Participants   "+ "   "])
        for question in poll:
            writer.writerow(["Question: "+ question.text])
            for stock in question.choice_set.all():
                 writer.writerow(["**"+stock.choice_text+"**"])
                 if Vote.objects.filter(choice__choice_text__contains=stock.choice_text).exists():
                     v=Vote.objects.filter(choice__choice_text=stock.choice_text)
                     for ss in v:
                        writer.writerow(["Participant Name: "+ss.participant.participent])
                 else:
                     writer.writerow(["No Participant Selected This Choice"])
        return response

@csrf_exempt
def signUp(request):
    str1 = ""
    if request.method == "POST":
        response_data = {}
        json_data = json.loads(request.body)
        first_name = json_data['fname']
        last_name = json_data['lname']
        username = json_data['username']
        email = json_data['email']
        # designation = json_data['designation']
        # cnic = json_data['cnic']
        password = json_data['password']
        print(first_name, last_name, username, email,  password)
        if User.objects.filter(username=username).exists():
            response_data = {
                'username': False,
                'email': True,

            }
            return JsonResponse(response_data)
        elif User.objects.filter(email=email).exists():
            response_data = {
                'username': True,
                'email': False,

            }
            return JsonResponse(response_data)
        else:
            request.session['password'] = password
            request.session['username'] = username
            request.session['first_name'] = first_name
            request.session['last_name'] = last_name
            request.session['email'] = email

            response_data = {
                'username': True,
                'email': True,

            }
            print('Done')
            fnum = randomly()
            request.session['fnum'] = fnum
            subject = "Please verify your email for poll : ARM"
            message = 'Welcome to poll : ARM !\nWe need to confirm your email address for completing your signup process at our poll : ARM Site.\nEnter the following verification password :\n' + fnum + '.\n\nThank You!'
            from_email = settings.EMAIL_HOST_USER
            to_email = email
            print(from_email)
            try:
                send_mail(subject, message, from_email, [to_email], fail_silently=False)
                print("done")
                return JsonResponse(response_data)
            except:
                return JsonResponse(response_data)
    return render(request, 'signup.html')

def register(request):
        if request.method == 'POST':
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            username = request.POST['username']
            password = request.POST['password']
            password1 = request.POST['password1']
            email = request.POST['email']
            if password == password1:
                if User.objects.filter(username=username).exists():
                    print('username is taken')
                    messages.success(
                        request, "Username is Taken",
                        extra_tags='alert alert-success alert-dismissible fade show')
                    return redirect('signup')
                elif User.objects.filter(email=email).exists():
                    print('Email eixsts')
                    messages.success(
                        request, "Email Already Exists",
                        extra_tags='alert alert-success alert-dismissible fade show')
                    return redirect('signup')
                else:
                    user = User.objects.create_user(username=username, password=password1, email=email,
                                                    first_name=first_name, last_name=last_name)
                    user.save()
                    print('User Created')
                    # messages.info(request, 'Registeration successfull')
                    return render(request, 'Login.html')
            else:
                messages.success(
                    request, "Password not matching",
                    extra_tags='alert alert-success alert-dismissible fade show')
                return redirect('signup')
        else:
            return render(request, 'signup.html')


@csrf_exempt
def confirm(request):
    from_email = settings.EMAIL_HOST_USER
    print(from_email)
    if request.method == "POST":
        fnum = request.session.get('fnum')
        json_data = json.loads(request.body)
        code = json_data['verification']
        print(code)
        print("done")
        if fnum == code:
            print("MATCH")
            password = request.session.get('password')
            username = request.session.get('username')
            first_name = request.session.get('first_name')
            last_name = request.session.get('last_name')
            email = request.session.get('email')
            # cnic = request.session.get('cnic')
            # designation = request.session.get('designation')
            user = User.objects.create_user(password=password, username=username, first_name=first_name,
                                            last_name=last_name, email=email)
            user.save()

            # user_profile = Profile.objects.get(user=user)
            # user_profile.CNIC = cnic
            # user_profile.grade = 0
            # user_profile.phone = 0
            # user_profile.save()
            print('USER_CREATED. . .')
            response_data = {
                'login': True,
            }
            print("AFTER SAVING")
            return JsonResponse(response_data)
        else:
            print("DON'T_MATCH")
            response_data = {
                'login': False,
            }
            return JsonResponse(response_data)
        return JsonResponse(response_data)
    return render(request, 'forgot.html')


@csrf_exempt
def forget_password(request):
    if request.method == "POST":
        json_data = json.loads(request.body)
        print(json_data)
        name = json_data['login']
        print(name)
        if User.objects.filter(username__icontains=name).exists():

                uemail = User.objects.get(username__icontains=name)
                email = uemail.email
                print("EMAIL: ", email)
                response = {
                    'login': True,
                    'email': email
                }
                fnum = randomly()
                request.session['fnum'] = fnum
                subject = "Foget Password Request"
                message = 'Hi ' + name + ',\nWe received a requested to reset your poll:ARM password.\nEnter the following password reset code:\n' + fnum + '\n\nThank You!'
                from_email = settings.EMAIL_HOST_USER
                to_email = email
                print("FROM: ", from_email, " TO: ", to_email)
                try:
                    print("SENDING")
                    send_mail(subject, message,from_email, [to_email], fail_silently=False)
                    print("EMAIL_SENT")
                    request.session['name'] = name
                    return JsonResponse(response)
                except:
                    return JsonResponse(response)
        else:
            response = {
                'login': False,
                'email': False
            }
            return JsonResponse(response)
    return render(request, 'forgot.html')

@csrf_exempt
def forget_done(request):
    if request.method == "POST":
        name = request.session.get('name')
        print(name)
        json_data = json.loads(request.body)
        password = json_data['password']
        print(password)
        if name.__contains__("@"):
            user = User.objects.get(email=name)
            user.set_password(password)
            print("UPDATED PASS: ", user.password)
            user.save()
            response = {
                'redirect': True
            }
            return JsonResponse(response)

        else:
            user = User.objects.get(username=name)
            user.set_password(password)
            print("UPDATED PASS: ", user.password)
            user.save()
            response = {
                'redirect': True
            }
            return JsonResponse(response)
    return render(request, 'forgot.html')
@csrf_exempt
def forget_confirm(request):
    num1 = random.randint(0, 9)
    num2 = random.randint(0, 9)
    num3 = random.randint(0, 9)
    num4 = random.randint(0, 9)
    fnum = str(num1) + str(num2) + str(num3) + str(num4)
    # print(from_email)
    if request.method == "POST":
        fnum = request.session.get('fnum')
        json_data = json.loads(request.body)
        code = json_data['verification']
        print(code)
        try:
            print("done")
            if fnum == code:
                print("MATCH")
                response_data = {
                    'login': True,
                }
                print("AFTER Dictionary")
                return JsonResponse(response_data)
            else:
                print("DON'T_MATCH")
                response_data = {
                    'login': False,
                }
                return JsonResponse(response_data)
        except:
            return HttpResponse("Bad Network")
    return render(request, 'forgot.html')


# @csrf_exempt
# def user_password_check(request):
#     user = User.objects.get(username=request.user.username)
#     response = {
#         'password': False
#     }
#     if request.method == "POST":
#         json_data = json.loads(request.body)
#         password1 = json_data['password']
#         print("USER_PASSWORD: ", password1)
#         if user.check_password(password1):
#             response = {
#                 'password': True
#             }
#             return JsonResponse(response)
#         else:
#             response = {
#                 'password': False
#             }
#             return JsonResponse(response)
#     return JsonResponse(response)
#
#
# @csrf_exempt
# def user_password_new_done(request):
#     user = User.objects.get(username=request.user.username)
#     if request.method == "POST":
#         response = {
#             'change_password': True
#         }
#         json_data = json.loads(request.body)
#         npass = json_data['new_password']
#         print("NEW password: ", npass)
#         user.set_password(npass)
#         user.save()
#         return JsonResponse(response)
#




@csrf_exempt
def login(request):
    # status = request.session.get('login')
    # if status:
    #     return redirect('Dashboard')
    if request.method == "POST":
        response_data = {}
        json_data = json.loads(request.body)
        print(type(json_data))
        username = json_data['login']
        password = json_data['password']
        print(username, password)
        user = auth.authenticate(username=username,password=password)
        if user is not None:
            if user.is_staff== False:
                auth.login(request,user)
                response_data = {
                    'username': True,
                    'password': True,
                }
                request.session['login'] = True
                return JsonResponse(response_data)
            else:
                response_data = {
                    'username': False,
                    'password': False,
                }
                request.session['login'] = False
                return JsonResponse(response_data)
        else:
            response_data = {
                'username': False,
                'password': False,
            }
            request.session['login'] = False
            return JsonResponse(response_data)

    return render(request, 'login.html')


#
# def login(request):
#         if request.method == 'POST':
#             username = request.POST['username']
#             password = request.POST['password']
#             user = auth.authenticate(username=username, password=password)
#             if user is not None:
#                 if user.is_staff == False:
#                     auth.login(request, user)
#                     return redirect("Dashboard")
#                 else:
#                     messages.info(request, 'You are not authorized to use this Login Panel')
#                     return redirect('Login')
#             else:
#                 messages.info(request, 'WRONG CREDENTIALS')
#                 return redirect('Login')
#         else:
#             return render(request, 'login.html')

@login_required(login_url="/accounts/Login")
def Dashborad(request):

    return render(request,"registration/dashboard.html")



def logout_user(request):
    p = poll_room.objects.filter(creator=request.user)
    for i in p:
        i.public = False
        i.save()
    auth.logout(request)
    request.session.clear()


    return redirect('home')


def randomly():
    num1 = random.randint(1, 9)
    num2 = random.randint(0, 9)
    num3 = random.randint(0, 9)
    num4 = random.randint(0, 9)
    num5 = random.randint(0, 9)
    num6 = random.randint(0, 9)
    fnum = str(num1) + str(num2) + str(num3) + str(num4)
    return fnum



def randomly1():
    num1 = random.randint(1, 9)
    num2 = random.randint(0, 9)
    num3 = random.randint(0, 9)
    num4 = random.randint(0, 9)
    num5 = random.randint(0, 9)
    num6 = random.randint(0, 9)
    fnum = str(num1) + str(num2) + str(num3) + str(num4) + str(num5) + str(num6)
    return fnum
@login_required(login_url="/accounts/Login")
def create_poll_room(request):

    if request.method == 'POST':

        form = PollRoomForm(request.POST)
        if form.is_valid():
            poll = form.save(commit=False)


            poll.code = 0
            poll.creator = request.user
            # code = form.cleaned_data['code']

            poll.save()



            messages.success(
                request, "Poll Successfully Created !",
                extra_tags='alert alert-success alert-dismissible fade show')
            print(poll.id)
            request.session['list_id'] = poll.id

            return redirect('List',poll.id)
    else:
        form = PollRoomForm()
    context = {
        'form': form,
    }
    return render(request, 'registration/test.html', context)

@csrf_exempt
def test(request):
    if request.GET.get('id'):
        id = request.GET.get('id')
        poll = get_object_or_404(poll_room, id=id)
        poll.start = True
        poll.save()
        data = {
        'code' : True,
        'status': True,

    }
        return  JsonResponse(data)


@csrf_exempt
def pollVote(request):
    data = {}
    if request.GET.get('d'):
        d =request.GET.get('d')
        poll = get_object_or_404(Poll,pk=d)
        loop_count = poll.choice_set.count()
        context = {
            'poll': poll,
            'loop_time': range(0, loop_count),
        }

        return render(request, 'Details-Poll.html', context)
    data = {
        'status': True,
        'multiSelect':True,
        'redirect':True,
    }
    return JsonResponse(data)

def pollResult(request):
    if request.GET.get('id'):
        id = request.GET.get('id')
        poll = get_object_or_404(Poll,id=id)
        return render(request, 'Results-Poll.html', {'poll': poll})


@csrf_exempt
def poll_vote_test(request,id):
  data = {'multiSelect':True,
                'redirect':True}
  if request.method == 'POST':
        choice_id = []
        json_data = json.loads(request.body)
        poll = get_object_or_404(Poll, pk=id)
        # choice_id= request.data['choice']
        # choice_id = request.GET.getlist('choice[]')
        for c in json_data['data']:
            choice_id.append(c)
        # choice_id.decode('utf-8')
        # choice_id = request.POST.get('choice')
        # print(choice_id.data)
        print(choice_id)
        if choice_id:
            for ch in choice_id:
                choice = Choice.objects.get(id=int(ch))
                x = request.session.get('parti_id')
                print("PARTICIPANT ID " + str(x))
                partcipant = get_object_or_404(Participents, pk=x)
                vote = Vote(poll=poll, choice=choice,participant=partcipant)


                # partcipant.choice_select=choice

                vote.save()
                print(vote)
            data = {
             'status': True,

                }
            return JsonResponse(data)
  return JsonResponse(data)



def testFunction(request):
    votedata = []
    choicedata=[]
    choicedata1 = []
    data = []
    test=[]
    if request.GET.get('id_poll'):
        id_poll = request.GET.get('id_poll')
        poll =get_object_or_404(Poll,pk=id_poll)
        x=poll.room.id
        room = get_object_or_404(poll_room,pk=x)
        pollCount = Poll.objects.filter(room_id__exact=x).last()
        print(pollCount.id)
        print("ID"+id_poll)
        if int(id_poll) == int(pollCount.id):
            room.save()
            for n in Poll.objects.filter(room=room):
                votedata = {n.text: n.get_vote_count}
            
            print(votedata)
            p = Poll.objects.filter(room=room)
            data = {"users": list(p.values()),}
            cmptd,created = PollCompleted.objects.get_or_create(room=room,code_room=room.code)
            cmptd.participants=Participents.objects.filter(room_code=room.code).count()
            cmptd.data=votedata
            cmptd.poll_data=data
            cmptd.save()
            #testing
            old_entry = QuestionsRecord.objects.filter(recorded_date=cmptd).delete()
            for question in Poll.objects.filter(room=room):
                votes = question.choice_set.all()
                QuestionsRecord(poll=question.text, vote_data=question.get_result_dict(),recorded_date=cmptd).save()
            return redirect('sessionEnd')
        elif id_poll !=pollCount.id:
            id_poll = int(id_poll) + 1
            poll = get_object_or_404(Poll, pk=id_poll)
            return render(request, 'Details-Poll.html', {'poll': poll})

        else:
            poll = get_object_or_404(Poll, pk=id_poll)
            return render(request, 'Details-Poll.html', {'poll': poll})


def viewCompletedPoll(request,id):
    questions_list = []
    choices_list =[]
    test_list =[]
    complete = PollCompleted.objects.get(id=id)
    c = complete.poll_data
    # print(c)
    for d in c['users']:
      questions_list.append(d['id'])
      print(questions_list)
        #c['users'][0]['id']
    p = Poll.objects.filter(id__in=questions_list)

    print(p)
    test_poll = QuestionsRecord.objects.filter(recorded_date=complete)
    return render(request,'PollResults.html',{'com':complete,'questions':test_poll})

def getCompletedPollData(request,id):
    complete = PollCompleted.objects.get(id=id)
    return JsonResponse(complete.data,safe=False)

@csrf_exempt
def goto_room(request):
    id=request.session.get('done_id')
    admin_id = request.session.get('admin_id')
    Room = poll_room.objects.get(id=id)

    ptcpn = Participents.objects.filter(p_room=Room)
    p = Poll.objects.filter(room=Room).first()
    data={
    'participant': ptcpn,
        'room':Room,
        'admin':admin_id,
        'p' : p
    }
    return render(request,'table.html',data)

@csrf_exempt
def getPublicize(request,id):
    room = get_object_or_404(poll_room,pk=id)
    room.public=True
    room.save()
    return redirect('created')


def getParticipents(request,id):
    room = get_object_or_404(poll_room,pk=id)
    recMesege = Participents.objects.filter(room_code__exact=room.code)

    # x=Participents.objects.filter(p_room__creator=request.user).count()
    count = Participents.objects.filter(room_code__exact=room.code).count()


    return JsonResponse({"users": list(recMesege.values()),"count": count,})


def getPublicizedPoll(request):
    room = poll_room.objects.filter(public=True).order_by('-pub_date')
    cate = room.values('category__catgry').order_by('-pub_date')

    # x=Categories.objects.values(cate)
    # print(x)
    return JsonResponse({"users": list(room.values()) , "cate" : list(cate)  } )

def getParticipantName(request,poll_id):
    code = poll_id
    if request.method == 'POST':
        if request.POST.get('check'):
           p = poll_room.objects.get(code__exact=code, active=True)
           part = Participents(p_room=p,participent="Anonymous",room_code=code)
           part.save()
           request.session['parti_id'] = part.id
           return redirect('clientJoin',p.id)
        else:
           name = request.POST['name']
           check = request.POST.get('check')
           code = poll_id
           p = poll_room.objects.get(code__exact=code, active=True)
           part = Participents(participent=name,p_room=p,room_code=code)

           part.save()
           request.session['parti_id'] = part.id
           return redirect('clientJoin',p.id)
    return render(request,'singlebox.html')
def doneRoom(request,id):
    room = get_object_or_404(poll_room,pk=id)
    if Poll.objects.filter(room=room).exists():
        return redirect('created')
    else:
        messages.success(
            request, "Poll Must Contain Atleast 1 Question",
            extra_tags='alert alert-success alert-dismissible fade show')
        # request.session['update_id']=room.id
        return redirect('List',room.id)

@csrf_exempt
def done_room(request,id):
        request.session['done_id']= id

        room = poll_room.objects.get(id=id)
        room.code = randomly1()
        room.start = True
        room.save()
        qstn = Poll.objects.filter(room=room)

        return redirect('created')

def poll_history(request,id):
    if request.method == 'POST':
        search_term = request.POST.getlist('to_compare')
        complete = PollCompleted.objects.get(id=search_term[0])
        questions = QuestionsRecord.objects.filter(recorded_date=complete)
        print(questions)
        questions_list = []
        # print(c)
        for d in search_term:
            questions_list.append(PollCompleted.objects.get(id=d))
        test_poll = QuestionsRecord.objects.filter(recorded_date__in=questions_list).order_by('-poll')
        
        print(test_poll)
        return render(request,'PollCompare.html',{'roomNum':id,'ql': questions_list,'com':complete,'questions': questions, 'poll':test_poll})

    p = Poll.objects.filter(owner=request.user)
    room = poll_room.objects.get(id=id)
    completed = PollCompleted.objects.filter(room=room,room__creator=request.user).order_by('-completed_date')
    print(request.user.first_name)

    return render(request,"2table.html",{'room':room, 'completed':completed})

def created_poll(request):
    if 'search' in request.GET:
        search_term = request.GET['search']
        all_polls = poll_room.objects.filter(title__icontains=search_term,creator=request.user,delete_poll=False)
        print(all_polls)
        completed = PollCompleted.objects.filter(room__creator=request.user).order_by('-completed_date')
        return render(request, "cr-table.html", {'room': all_polls, 'completed': completed})
    p = Poll.objects.filter(owner=request.user)
    completed = PollCompleted.objects.filter(room__creator=request.user).order_by('-completed_date')
    room = poll_room.objects.filter(creator=request.user,delete_poll=False).order_by('-pub_date')
    print(request.user.first_name)
    print(room)


    return render(request,"cr-table.html",{'room'  : room,'completed':completed})

def savePoll(request , id):
    room = get_object_or_404(poll_room,pk=id)
    if room.active:
        room.save_poll = True
        room.active = False
        room.start = False
        room.save()
        return redirect('Dashboard')




def clientJoin(request,id):
    p = poll_room.objects.get(id=id)
    count = Participents.objects.filter(p_room=p).count()
    no_particpent = Participents.objects.filter(p_room=p)
    question = Poll.objects.filter(room=p).first()
    pp = request.session.get('parti_id')
    print(pp)
    data = {
        'p': p,
        'part': no_particpent,
        'count': count,
        'p_id': pp,
        'question':question,
    }
    return render(request,'Join.html',data)




@csrf_exempt
def PollRoomJoin(request,poll_id):
        print(poll_id)
    # if request.method == 'POST':
        code=poll_id
        p = poll_room.objects.get(code__exact=code, active=True,)
        q = Poll.objects.filter(room=p)
        count = Participents.objects.filter(p_room=p).count()

        return redirect('clientJoin',p.id)
    # else:
    #     return render(request,'JoinPoll.html')

@csrf_exempt
def participentLeft(request):
    data = {}
    if request.method == "POST":
        json_data = json.loads(request.body)
        # x = request.session.get('parti_id')

        id = json_data['participent_id']
        print(id)
        if id is not None:
            d=get_object_or_404(Participents,pk=id)
            d.delete()
            return JsonResponse(data)
    return JsonResponse(data)

def getMultiSelect(request,id):
    data = {}
    p = get_object_or_404(Poll,pk=id)
    if p.multiSelect:
        data = {
            'multiSelect':True,
            'redirect':True
        }
        return JsonResponse(data)
    else:
        data = {
            'multiSelect':False,
            'redirect':True
        }
        return JsonResponse(data)

@csrf_exempt
def join_poll(request,poll_id):
    response = {}
    check = {}
    contains_digit = False
    if request.method == 'POST':
       json_data = json.loads(request.body)
       room_code = poll_id
       request.session['join_code'] = room_code
       print(room_code)
       # s=isinstance(room_code, str)
       # print(s)
       if poll_room.objects.filter(code__exact=room_code,active=True).exists():
           response = {
            'code': True
               }
           print("TRUE")
           return JsonResponse(response)
       else:
        response = {
         'code': False
                   }
        print("False")
        return JsonResponse(response)
    print("Nothing")
    return JsonResponse(response)

def startRoom(request,id):
    s = get_object_or_404(poll_room,pk=id)
    s.start = True
    s.save()
    return redirect('Dashboard')

def endRoom(request,id):
    s = get_object_or_404(poll_room, pk=id)
    s.active = False
    s.start = False
    s.save_poll=False
    s.save()
    return redirect('resultsData',id)


def resultsData(request, id):
    votedata = []

    question = Poll.objects.get(id=id)

    votes = question.choice_set.all()

    for i in votes:
        votedata.append({i.choice_text: i.get_vote_count})

    print(votedata)
    return JsonResponse(votedata, safe=False)



def leave(request,id):
    leaveRoom = get_object_or_404(Participents,pk=id)
    leaveRoom.delete()
    return redirect('/')
@login_required(login_url="/accounts/Login")
def create_poll(request,id):
    poll_id = poll_room.objects.get(id=id)
    category = poll_id.category
    print(category)
    pass_polls = poll_room.objects.filter(category=category, creator=request.user)
    print(pass_polls)
    pass_questions = Poll.objects.none()
    for q in pass_polls:
        pass_questions = pass_questions | Poll.objects.filter(room = q.id)

    if request.method == 'POST':
        form = PollAddForm(request.POST)
        if form.is_valid():
            poll = form.save(commit=False)
            poll.owner = request.user
            poll.room = poll_id
            poll.save()
            new_choice1 = Choice(
                poll=poll, choice_text=form.cleaned_data['choice1']).save()
            new_choice2 = Choice(
                poll=poll, choice_text=form.cleaned_data['choice2']).save()
            if form.cleaned_data['choice3']:
                new_choice3 = Choice(
                    poll=poll, choice_text=form.cleaned_data['choice3']).save()
            if form.cleaned_data['choice4']:
                new_choice4 = Choice(
                    poll=poll, choice_text=form.cleaned_data['choice4']).save()

            messages.success(
                request, "Questions added successfully",
                extra_tags='alert alert-success alert-dismissible fade show')
            request.session['update_id']=poll_id.id
            print(poll_id)
            return redirect('List',poll_id.id)
    else:
        form = PollAddForm()
    context = {
        'pass_questions': pass_questions,
        'form': form,
        'poll':poll_id
    }
    return render(request, 'Addpoll.html', context)
@login_required(login_url="/accounts/Login")
def account_info(request):
    return render(request,"AccountInfo.html")


def deletePollRoom(request,id):
    room = get_object_or_404(poll_room,pk=id)
    room.start=False
    room.public=False
    room.delete_poll=True
    room.save()
    return redirect('created')


def getPollList(request):
    pass


# @login_required(login_url="/accounts/Login")
def polls_list(request,id):
    if request.GET.get('d'):
        d = request.GET.get('d')
        room = poll_room.objects.get(id=d)
        p = Poll.objects.filter(room_id__exact=room.id)

        context = {

            'room': room,
            'p': p,
        }
        return render(request, 'ListPoll.html', context)
    room = poll_room.objects.get(id=id)
    p = Poll.objects.filter(room_id__exact=room.id)

    context = {

        'room': room,
        'p': p,
    }
    return render(request, 'ListPoll.html', context)

    # if request.session.get('update_id'):
    #     update_id = request.session.get("update_id")
    #     print("Update LIST ID " + str(update_id))
    #     room = poll_room.objects.get(id=update_id)
    #     p = Poll.objects.filter(room_id__exact=room.id)
    #
    #
    #     context = {
    #
    #         'room': room,
    #         'p': p,
    #     }
    #     return render(request, 'ListPoll.html', context)
    # else:
    #     id = request.session.get("list_id")
    #     print("LIST ID "+str(id))
    #     update_id = request.session.get("update_id")
    #     print("Update LIST ID " + str(update_id))
    #     room = poll_room.objects.get(id=id)
    #     p = Poll.objects.filter(room_id__exact=room.id)
    #     request.session.modified = True
    #     # request.session.clear_expired()
    #     context = {
    #         # 'polls': polls,
    #         # 'params': params,
    #         # 'search_term': search_term,
    #         'room' : room,
    #         'p' : p,
    #     }
    #     return render(request, 'ListPoll.html', context)

# @login_required(login_url="/accounts/Login")
def poll_detail(request, poll_id):
    poll = get_object_or_404(Poll, id=poll_id)

    if not poll.active:
        return render(request, 'Results-Poll.html', {'poll': poll})
    loop_count = poll.choice_set.count()
    context = {
        'poll': poll,
        'loop_time': range(0, loop_count),
    }
    return render(request, 'Details-Poll.html', context)


def getPollRecord(request,id):
    poll = PollCompleted.objects.get(id=id)
    return JsonResponse(poll.poll_data,safe=False)

def getPollChoicesRecord(request,id):

    poll = PollCompleted.objects.get(id=id)
    print(poll.choice_data)
    # decodedSet = jsonpickle.decode(poll.choice_data)
    # print(decodedSet)
    # result = json.dumps(list(poll.choice_data))
    return JsonResponse(poll.choice_data,safe=False)

@login_required(login_url="/accounts/Login")
def polls_edit(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    if request.user != poll.owner:
        return redirect('home')

    if request.method == 'POST':
        form = EditPollForm(request.POST, instance=poll)
        if form.is_valid():
            form.save()
            messages.success(request, "Question Updated successfully",
                             extra_tags='alert alert-success alert-dismissible fade show')
            request.session['update_id']=poll.room.id
            return redirect("List",poll.room.id)

    else:
        form = EditPollForm(instance=poll)

    return render(request, "Edit-Poll.html", {'form': form, 'poll': poll})


def polls_delete(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    if request.user != poll.owner:
        return redirect('home')
    poll.delete()
    messages.success(request, "Poll Deleted successfully",
                     extra_tags='alert alert-success alert-dismissible fade show')
    return redirect("List",poll.room.id)



def choice_delete(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    poll = get_object_or_404(Poll, pk=choice.poll.id)
    if request.user != poll.owner:
        return redirect('home')
    choice.delete()
    messages.success(
        request, "Option Deleted successfully", extra_tags='alert alert-success alert-dismissible fade show')
    return redirect('edit', poll.id)



@login_required
def add_choice(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    if request.user != poll.owner:
        return redirect('home')

    if request.method == 'POST':
        form = ChoiceAddForm(request.POST)
        if form.is_valid():
            new_choice = form.save(commit=False)
            new_choice.poll = poll
            new_choice.save()
            messages.success(
                request, "Option added successfully", extra_tags='alert alert-success alert-dismissible fade show')
            return redirect('edit', poll.id)
    else:
        form = ChoiceAddForm()
    context = {
        'form': form,
        'id' : poll_id,
    }
    return render(request, 'Choice-Add.html', context)
#
# def seeGraph(request,id):
#     cmpt = PollCompleted.objects.get(id=id)
#     return render(request,'Graph.html',{'cmt':cmpt})

@login_required
def choice_edit(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    poll = get_object_or_404(Poll, pk=choice.poll.id)
    if request.user != poll.owner:
        return redirect('home')

    if request.method == 'POST':
        form = ChoiceAddForm(request.POST, instance=choice)
        if form.is_valid():
            new_choice = form.save(commit=False)
            new_choice.poll = poll
            new_choice.save()
            messages.success(
                request, "Option Updated successfully", extra_tags='alert alert-success alert-dismissible fade show')
            return redirect('choice_edit', poll.id)
    else:
        form = ChoiceAddForm(instance=choice)
    context = {
        'form': form,
        'edit_choice': True,
        'choice': choice,
    }
    return render(request, 'Choice-Add.html', context)

def resultsData1(request, id):
    poll = Poll.objects.filter(room_id__exact=id).first()
    count = PollCompleted.objects.filter(room_id__exact=id).count()
    complete = PollCompleted.objects.filter(room_id__exact=id)
    part = Participents.objects.filter(p_room_id=id).count()
    return render(request,"View Data.html",{'poll':poll,'count':count,'part': part,'complete':complete})


def getParticipantsChoice(request):
    if request.GET.get('choice_id') and request.GET.get('poll_id'):
        choice_id = request.GET.get('choice_id')
        poll_id = request.GET.get('poll_id')
        ch = Choice.objects.get(id=choice_id)
        # poll = Poll.objects.filter(choice__id)
        poll = get_object_or_404(Poll,pk=poll_id)
        count = PollCompleted.objects.filter(room=poll.room).count()
        complete = PollCompleted.objects.filter(room=poll.room)
        vote = Vote.objects.filter(choice_id__exact=choice_id)
        part = Participents.objects.filter(p_room_id=poll.room).count()
        return render(request,'View Data.html',{'vote':vote,'poll':poll,'ch':ch,'cmt':complete,'complete':complete,'count':count,'part':part})
    elif request.GET.get('record_id') and request.GET.get('id'):
        record_id=request.GET.get('record_id')
        poll_id = request.GET.get('id')
        complete = PollCompleted.objects.get(id=record_id)

        poll = get_object_or_404(Poll, pk=poll_id)
        count = PollCompleted.objects.filter(room=poll.room).count()
        complete1 = PollCompleted.objects.filter(room=poll.room)
        part = Participents.objects.filter(p_room_id=poll.room).count()
        return render(request, 'View Data.html', { 'poll': poll, 'cmt': complete,'complete':complete1,'count':count,'part':part})

def compareGraph(request,id):
    votedata = []
    cmpt = PollCompleted.objects.get(id=id)
    votedata = cmpt.data
    return JsonResponse(votedata,safe=False)

def previousQuestion(request):
    if request.GET.get('id'):
        id = request.GET.get('id')
        p = get_object_or_404(Poll,pk=id)
        t =  Poll.objects.filter(room=p.room)
        questions = Poll.objects.filter(room=p.room).first()
        if p.id!=questions.id:
            poll = p.previous()
            count = PollCompleted.objects.filter(room=p.room).count()
            part = Participents.objects.filter(p_room_id=p.room).count()
            c = PollCompleted.objects.filter(room=p.room)
            return render(request, "View Data.html", {'poll': poll,'count':count,'part':part,'complete':c})

        elif p.id==questions.id:
             return redirect('history')

        return redirect('history')


def getAccurateResults(request,id):
    votedata = []
    question = Poll.objects.get(id=id)
    votes = question.choice_set.all()

    for i in votes:
        votedata.append({i.choice_text: i.get_vote_count})

    print(votedata)
    return JsonResponse({"users":list(question.get_result_dict()),"count":question.get_vote_count}, safe=False)

def nextQuestion(request):
    if request.GET.get('id'):
        id = request.GET.get('id')
        p = get_object_or_404(Poll,pk=id)

        # print(poll)
        t =  Poll.objects.filter(room=p.room)

        questions = Poll.objects.filter(room=p.room).last()
        if p.id!=questions.id:
            poll = p.next()
            c = PollCompleted.objects.filter(room=p.room)
            count =PollCompleted.objects.filter(room=p.room).count()
            part = Participents.objects.filter(p_room_id=p.room).count()
            return render(request, "View Data.html", {'poll': poll,'complete':c,'count':count,'part':part})
            # else:
            #     return redirect('history')
        elif p.id==questions.id:
             return redirect('history')
        # if int(poll.id) != int(questions.id)  :
        #     poll = get_object_or_404(Poll,pk=int(id)+1)
        #     print(int(id))
        #     print(str(questions.id) + str(questions.text))
        #     return render(request, "View Data.html", {'poll': poll})
        # else:
        #     return redirect("history")
        # return render(request, "View Data.html", {'poll': poll})
        return redirect('history')
def msge2(request,id):
    data = {}
    room = poll_room.objects.get(id=id)
    # room = get_object_or_404(poll_room,pk=id)
    if room.start:
        data = {
        'room' : True
              }
        return JsonResponse(data)
    return JsonResponse(data)



def end_pollRoom(request,id):
    proom = poll_room.objects.get(id=id)
    poll = Poll.objects.filter(room=proom)
    print(poll)
    data = {
        'poll': poll,
        # 'ch': ch,

    }
    if proom.active is True:
        proom.delete()
        # return redirect('resultsData',id)
        return redirect('Dashboard')






@login_required
def choice_delete(request, choice_id):
    choice = get_object_or_404(Choice, pk=choice_id)
    poll = get_object_or_404(Poll, pk=choice.poll.id)
    if request.user != poll.owner:
        return redirect('home')
    choice.delete()
    messages.success(
        request, "Option Deleted successfully", extra_tags='alert alert-success alert-dismissible fade show')
    return redirect('edit', poll.id)



def poll_vote(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)

    choice_id = request.POST.get('choice')
    if choice_id:
        choice = Choice.objects.get(id=choice_id)
        vote = Vote( poll=poll, choice=choice)
        vote.save()
        print(vote)
        return render(request, 'Results-Poll.html', {'poll': poll,'room': poll.room.id})
    else:
        messages.error(
            request, "No Option selected", extra_tags='alert alert-warning alert-dismissible fade show')
        return redirect("detail", poll_id)
    return render(request, 'Results-Poll.html', {'poll': poll,'room': poll.room.id})


def sessionEnd(request):
    if request.user.is_authenticated:
        return redirect('Dashboard')
    else:
        return redirect('home')

def AjaxGetQuestion(request):
    question = request.GET.get('question')
    poll_obj = Poll.objects.get(id=question)
    choices_filter = Choice.objects.filter(poll=poll_obj)
    choices = choices_filter.values('choice_text','poll__text')
    choice = json.dumps(list(choices))

    data = {'choice':choice,
        'status':'success'}

    return JsonResponse(data)
    
def AjaxGetPollDetails(request):
    id = request.GET.get('id')
    room = poll_room.objects.get(id=id)
    cat = room.category

    data = {'status':'success'}
    data['title'] = room.title
    data['dscp'] = room.dscp
    data['start'] = room.start
    data['code'] = room.code
    data['category'] = str(cat)
    
    return JsonResponse(data)

@csrf_exempt
def close_room(request,id):
        request.session['done_id']= id

        room = poll_room.objects.get(id=id)
        room.public=False
        room.start = False
        room.save()
        qstn = Poll.objects.filter(room=room)

        return redirect('created')

@csrf_exempt
def UnlistPoll(request,id):
    room = get_object_or_404(poll_room,pk=id)
    room.public=False
    room.save()
    return redirect('created')

def poll_record(request):
    if 'search' in request.GET:
        search_term = request.GET['search']
        all_polls = poll_room.objects.filter(title__icontains=search_term,creator=request.user,delete_poll=False)
        print(all_polls)
        completed = PollCompleted.objects.filter(room__creator=request.user).order_by('-completed_date')
        return render(request, "cr-record.html", {'room': all_polls, 'completed': completed})
    p = Poll.objects.filter(owner=request.user)
    completed = PollCompleted.objects.filter(room__creator=request.user).order_by('-completed_date')
    room = poll_room.objects.filter(creator=request.user,delete_poll=False).order_by('-pub_date')
    print(request.user.first_name)
    print(room)


    return render(request,"cr-record.html",{'room'  : room,'completed':completed})