from django.contrib import admin
from .models import *
# Register your models here.
admin.site.site_header = "poll:ARM Admin"
admin.site.site_title = "poll:ARM Admin"
admin.site.index_title = "Welcome to the poll:ARM Admin"


# class ChoiceInLine(admin.TabularInline):
#     model = Choice
#     extra = 4
#
# class QuestionAdmin(admin.ModelAdmin):
#     fieldsets = [(None,{'fields': ['question_text']})
#                  ,]
#     inlines = [ChoiceInLine]




# admin.site.register(Question,QuestionAdmin)

admin.site.register(Vote)
admin.site.register(Choice)
admin.site.register(Poll)
admin.site.register(poll_room)
admin.site.register(Categories)
admin.site.register(PollCompleted)
admin.site.register(Participents)
admin.site.register(QuestionsRecord)