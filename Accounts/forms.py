from django import forms
from django.core import validators
from django.core.exceptions import ValidationError
import re
from .models import Poll, Choice,poll_room,Categories


class PollRoomForm(forms.ModelForm):

    title  = forms.CharField(label='Poll Title', max_length=500, min_length=2,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))



    class Meta:
        model = poll_room
        fields = ['title', 'dscp', 'category'  ]
        labels = {
            'dscp': ('Description '),
            'category': ('Category:')
        }
        widgets = {
            'dscp': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20,}),
        }

    # def clean_msg(self):
    #     msg = self.cleaned_data['title']
    #     if re.match('/[\s]+$', msg):
    #         raise forms.ValidationError("Spaces not allowed")
    #     return msg



    # def validate_not_spaces(value):
    #     if isinstance(value, str) and value.strip() == '':
    #         raise ValidationError(u"You must provide more than just whitespace.")
    # def clean_title(self):
    #     name1 = self.cleaned_data['title']
    #     if ' ' in name1:
    #         raise forms.ValidationError('White spaces test')
    #     return name1

class PollAddForm(forms.ModelForm):

    choice1 = forms.CharField(label='Option 1', max_length=100, min_length=2,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    choice2 = forms.CharField(label='Option 2', max_length=100, min_length=2,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    choice3 = forms.CharField(label='Option 3', max_length=100, min_length=2,
                              widget=forms.TextInput(attrs={'class': 'form-control'}),required=False)
    choice4 = forms.CharField(label='Option 4', max_length=100, min_length=2,
                              widget=forms.TextInput(attrs={'class': 'form-control'}),required=False)

    class Meta:
        model = Poll
        fields = ['text', 'choice1', 'choice2', 'choice3', 'choice4', 'time','multiSelect']
        labels = {
            'text' : ('Question'),
            'multiSelect': ('Enable MultiSelect Option: ')
        }
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20}),
            'time': forms.NumberInput(attrs={'style': 'width: 100px','min':5, 'max':30,'value':5}),
        }


    # def clean_time(self):
    #     time = self.cleaned_data.get("time")
    #     if int(time) <= 30 and int(time)>=0:
    #         return time
    #     else:
    #         raise forms.ValidationError("Range should be withing 0 to 30 seconds")
class EditPollForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ['text', 'multiSelect']
        labels = {
            'text' : ('Question'),
        }
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20}),
        }


# class AddCategoryForm(forms.ModelForm):
#     class Meta:
#         model = Categories
#         fields = ['text', ]
#         labels = {
#             'text' : ('Question'),
#         }
#         widgets = {
#             'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 5, 'cols': 20}),
#         }


class ChoiceAddForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['choice_text', ]
        labels = {
            'choice_text' : ('Option:')
        }
        widgets = {
            'choice_text': forms.TextInput(attrs={'class': 'form-control', })
        }
