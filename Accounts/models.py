import secrets
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from picklefield.fields import PickledObjectField
# from main.models import poll
# Create your models here.
class Categories(models.Model):
    catgry = models.CharField(max_length=100)
    def __str__(self):
        return self.catgry
class poll_room(models.Model):
    creator = models.ForeignKey(User,related_name='+',on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    dscp = models.TextField()
    code = models.IntegerField()
    active = models.BooleanField(default=True)
    start = models.BooleanField(default=False)
    pub_date = models.DateTimeField(default=timezone.now)
    delete_poll = models.BooleanField(default=False)
    public= models.BooleanField(default=False)
    category = models.ForeignKey(Categories,related_name='+',on_delete=models.CASCADE)

    def __str__(self):
        return self.title



class Poll(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    pub_date = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)
    time = models.IntegerField(default=1, validators=[MinValueValidator(5), MaxValueValidator(30),],)
    room = models.ForeignKey(poll_room,related_name='+',on_delete=models.CASCADE)
    multiSelect= models.BooleanField(default=False)
    @property
    def get_vote_count(self):
        return self.vote_set.count()

    def get_result_dict(self):
        res = []
        for choice in self.choice_set.all():
            d = {}
            alert_class = ['primary', 'secondary', 'success',
                           'danger', 'dark', 'warning', 'info']

            d['alert_class'] = secrets.choice(alert_class)
            d['text'] = choice.choice_text
            d['num_votes'] = choice.get_vote_count
            if not self.get_vote_count:
                d['percentage'] = 0
            else:
                d['percentage'] = (choice.get_vote_count /
                                   self.get_vote_count)*100

            res.append(d)
        return res

    def __str__(self):
        return self.text

    def next(self):
        try:
            return Poll.objects.get(pk=self.pk + 1)
        except:
            return None

    def previous(self):
        try:
            return Poll.objects.get(pk=self.pk - 1)
        except:
            return None



class Choice(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=255)

    @property
    def get_vote_count(self):
        return self.vote_set.count()

    def __str__(self):
        return f"{self.poll.text[:25]} - {self.choice_text[:25]}"


class Participents(models.Model):
    participent = models.CharField(max_length=100, default="Anonymous")
    p_room = models.ForeignKey(poll_room, related_name='+', on_delete=models.CASCADE)
    room_code = models.IntegerField()


    def __str__(self):
        return self.participent
class Vote(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    participant = models.ForeignKey(Participents,related_name='Participant',on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.poll.text[:15]} - {self.choice.choice_text[:15]}'


class PollCompleted(models.Model):
    room = models.ForeignKey(poll_room,related_name='+',on_delete=models.CASCADE)
    completed_date = models.DateTimeField(default=timezone.now)
    code_room = models.IntegerField()
    data =PickledObjectField(null=True)
    poll_data =PickledObjectField(null=True)
    # choice_data = PickledObjectField()
    participants = models.IntegerField(null=True)
    def __str__(self):
        return self.room.title


class QuestionsRecord(models.Model):
    recorded_date = models.ForeignKey(PollCompleted,related_name='+',on_delete=models.CASCADE)
    poll = models.TextField(null=True)
    vote_data = PickledObjectField(null=True)