window.addEventListener('load', function () {

	//Shorthand query Selectors
	var $ = document.querySelector.bind(document),
		$$ = document.querySelectorAll.bind(document),
		box = $('#chatbox-body'),
		log_name = $('.user-profile a').innerHTML,
		check = 0,
		count = 0,
		temp = [],
		i = 0;

	//display functions
	function showf(elm) {
		elm.style.display = 'flex';
	}

	function hide(elm) {
		elm.style.display = 'none';
	}

	//function to clear the chat
	function clear(elm) {
		elm.innerHTML = '';
	}

	//add required classes to new text and append them
	function add_class(time, text, node, user) {
		time.classList.add('chatbox-chat-time');
		text.classList.add('chatbox-chat-message');
		node.classList.add('chatbox-chat-wrapper');
		if (user === log_name) {
			text.classList.add('chat-primary');
			node.classList.add('chat-primary-wrapper');
		} else
			text.classList.add('chat-secondary');

		//Append the children
		node.appendChild(text);
		node.appendChild(time);
		box.appendChild(node);
		node.scrollIntoView();
	}

	//function to check the input and post it
	function check_input(val) {
		if (val !== '') {
			var node = document.createElement('li'),
				text = document.createElement('span'),
				time = document.createElement('span'),
				date = new Date();

			time.innerHTML = date.toLocaleTimeString();
			text.innerHTML = val;

			//Add the required classes and append the child
			add_class(time, text, node, log_name);

			//send message to the server
			var url = '/Save-Chat/',
				data = {
					text: val,
					time: time.innerHTML,
					receiver: $('#chatbox-header-user span').innerHTML
				},
				http = new XMLHttpRequest();

			http.open('POST', url, true);
			http.setRequestHeader('Content-Type', 'Application/json');
			http.send(JSON.stringify(data));
		}
	}

	function append_text(arr) {
		var node = document.createElement('li'),
			text = document.createElement('span'),
			time = document.createElement('span'),
			user = arr[2];

		text.innerHTML = arr[0];
		time.innerHTML = arr[1];
		add_class(time, text, node, user);
	}

	//splits the incoming array
	function split_arr(arr) {
		count = arr.length, i = 0;
		for (; i < count; i++) {
			append_text(arr[i].split('~~'));
		}
	}

	//header info menu functionality
	{
		var btn1 = $('#trigger-header-menu-btn'),
			menu1 = $('#chatbox-header-info-menu');

		btn1.onclick = function () {

			if (check == 0) {
				showf(menu1);
				check = 1;
			} else {
				hide(menu1);
				check = 0;
			}

			var i = 0;
			var count = menu1.children.length;

			for (; i < count; i++) {
				//Clicking menu items hides parent element
				menu1.children[i].onclick = function () {
					hide(menu1);
					check = 0;
				};
			}
		};
	}

	//contacts menu functionality
	{
		var btn2 = $('#chatbox-footer-contacts'),
			btn3 = $('#chatbox-contacts-footer i'),
			menu2 = $('#chatbox-contacts'),
			btn5 = $('#no-message-here i'),
			box2 = $('#chatbox-contacts-body'),
			error2 = $('#chatbox-contacts-error');

		btn2.onclick = function () {
			showf(menu2);
		};
		btn3.onclick = function () {
			hide(menu2);
		};
		btn5.onclick = function () {
			showf(menu2);
		};
		if (box2.children.length === 1) {
			showf(error2);
		}
	}

	//chatbox contacts to chatbox header
	{
		var contact = $$('.chatbox-contact'),
			chatbox_user = $('#chatbox-header-user'),
			img1 = chatbox_user.children[0],
			name1 = chatbox_user.children[1],
			error1 = $('#no-message-here'),
			input1 = $('input[name=message]'),
			count = contact.length,
			i = 0;

		input1.value = '';

		//fetches previous messages from web server
		function fetch_msg(username) {

			var url = '/Chat-Box/',
				data = {
					username: username
				},
				http = new XMLHttpRequest(),
				output = '';

			http.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					output = JSON.parse(this.responseText);
					if (output.check === true) {
						split_arr(output.texts);
					}
				}
			};

			http.open('POST', url, true);
			http.setRequestHeader('Content-Type', 'Application/json');
			http.send(JSON.stringify(data));

		}

		//which contact is clicked?
		for (; i < count; i++) {
			contact[i].onclick = function () {
				//this one is clicked

				//Neccessary removals and focus etc.
				hide(this.parentElement.parentElement);
				hide(error1);
				input1.removeAttribute('readonly');
				input1.focus();

				//selected contact's stuff
				var img2 = this.children[0],
					name2 = this.children[1].children[0];

				clear(box);

				//assigning selected stuff to header
				name1.innerHTML = name2.innerHTML;
				img1.src = img2.src;

				//fetch (if any) previous messages from server
				fetch_msg(name2.innerHTML);
			};
		}
	}

	//Append new message to chatbox body
	{
		var input2 = $('input[name=message]'),
			btn4 = $('#chatbox-footer-send');

		//whether enter is pressed or send button is pressed
		btn4.onclick = function () {
			check_input(input2.value);
			input2.value = '';
		};
		input2.onkeypress = function (e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				check_input(input2.value);
				input2.value = '';
			}
		}
	}

	//Switch themes
	{
		//get the root containing the variables
		var root = document.documentElement,
			old_text = getComputedStyle(root).getPropertyValue('--text-color'),
			old_back = getComputedStyle(root).getPropertyValue('--back-color'),
			old_secd = getComputedStyle(root).getPropertyValue('--secd-color'),
			old_tert = getComputedStyle(root).getPropertyValue('--tert-color'),
			btn6 = $('#chatbox-theme-btn');

		btn6.onclick = function () {
			//when theme is dark
			if (check !== 1) {
				root.style.setProperty('--text-color', old_back);
				root.style.setProperty('--back-color', old_text);
				root.style.setProperty('--secd-color', old_tert);
				root.style.setProperty('--tert-color', old_secd);
				btn6.setAttribute('title', 'Switch to dark mode');
				check = 1;
			} //when theme is light
			else {
				root.style.setProperty('--text-color', old_text);
				root.style.setProperty('--back-color', old_back);
				root.style.setProperty('--secd-color', old_secd);
				root.style.setProperty('--tert-color', old_tert);
				btn6.setAttribute('title', 'Switch to light mode');
				check = 0;
			}
		};
	}

	//Remove repeating conversations
	{
		var names = $$('.chatbox-contact-uname'),
			count = names.length,
			temp = '',
			check = 0,
			i = 0,
			j = 0;

		for (; i < count; i++) {
			for (j = i + 1; j < count; j++) {
				if (i !== j) {
					if (names[i].innerHTML === names[j].innerHTML) {
						names[j].parentElement.parentElement.remove();
					}
				}
			}
		}
	}
});
