window.addEventListener('load', () => {

	var $ = document.querySelector.bind(document),
		form = $('form[name=code-form]'),
		input = $('input[name=code]');

	form.reset();

	function show(elm) {
		elm.style.display = 'block';
	}

	function showf(elm) {
		elm.style.display = 'flex';
	}

	function hide(elm) {
		elm.style.display = 'none';
	}

	$('#close-parent').onclick = () => {
		hide($('#code'));
		input.value = null;
	}

	$('#code-btn').onclick = () => {
		show($('#code'));
		input.focus();
	}

	$('#code-error').onclick = () => {
		hide($('#code-error'));
	}

	$('form[name=code-form] input').onkeydown = function (e) {
		var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'Enter', 'Backspace', 'Control', 'F5'],
			check = false;
		for (let index of arr) {
			if (e.key == index) {
				check = true;
			}
		}

		if (!check) {
			e.preventDefault();
		}
	}

	form.onsubmit = event => {
		event.preventDefault();
		var data = {
				'code': input.value
			},
			//insert target url
			url = location.protocol + '//' + location.host + '/accounts/JoinPoll/' + input.value + '/',

			http = new XMLHttpRequest();
		http.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				console.log("WTFF");
				var output = JSON.parse(this.responseText);

				//send a dictionary having a key named 'code'
				//send true if code matches and false if it doesnt

				if (output.code == true) {
					console.log("WTFF");
					location.href = '/accounts/Enter-your-Name/' + input.value + '/'; //redirection url
					console.log("WTFF");
				} else {
					showf($('#code-error'));
					// setTimeout(() => {
					// 	hide($('#code-error')), 3000
					// });
				}
			}
		}
		http.open('POST', url, true);
		http.setRequestHeader('Content-type', 'Application/json');
		http.send(JSON.stringify(data));
	}

});
