window.addEventListener('load', function () {
	//Shorthand Query Selectors
	var $ = document.querySelector.bind(document),
		$$ = document.querySelectorAll.bind(document),
		app_title = $('.app-title'),
		app_bg = $('.app-bg'),
		nav_db = $('.dashboard-nav'),
		logout_btn = $('.logout-button'),
		profile_btn = $('.user-profile'),
		add_btn = $('.add-application'),
		love = $('#love-django'),
		notify_file = '',
		close_notify = '',
		i = 0,
		url = '',
		data = {};


	//redirects to dashboard
	function dashboard() {
		location.href = '/accounts/Dashboard/';
	}

	function show(elm) {
		elm.style.display = 'flex';
	}

	function block(elm) {
		elm.style.display = 'block';
	}

	function hide(elm) {
		elm.style.display = 'none';
	}

	function fadeIn(elm) {
		elm.classList.remove('opacity-off', 'z-z-10');
	}

	function fadeOut(elm) {
		elm.classList.add('opacity-off', 'z-z-10');
	}

	nav_db.children[3].onmouseover = function () {
		block(love)
	};

	nav_db.children[3].onmouseout = function () {
		hide(love)
	};

	check = 0
	nav_db.children[3].onclick = function () {
		if (check === 0) {
			block(love);
			check = 1;
		} else {
			hide(love);
			check = 0;
		}
	};

	var node = document.createElement('div');
	node.setAttribute('id', 'completion-check');
	node.setAttribute('style', 'cursor: pointer');
	node.classList.add('f-big');
	$('.dashboard').appendChild(node);

	node.onclick = function () {
		hide(this);
	};


	//Redirect to chat page
	$('#open-chat-page').onclick = function () {
		location.href = '/Chat-Box';
	};

	//Menu buttons in action
	$('.menu-button').onclick = function () {
		$('.left-bar').style.display = 'flex';
	};
	$('.menu-close').onclick = function () {
		$('.left-bar').style.display = 'none';
	};

	//logout button in action
	logout_btn.onclick = function () {
		location.href = '/accounts/Logout';
	};

	//Click to open profile
	profile_btn.onclick = function () {
		location.href = '/User-Profile';
	};

	//click to open new application page
	add_btn.onclick = function () {

		var url = '/accounts/Create-pollRoom/',
			data = {
				check: true
			},
			http = new XMLHttpRequest();

		http.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				var output = JSON.parse(this.responseText);
				if (output.designation == false) {
					node.innerHTML = 'You do not have a designation, Please contact at Runtime.Terror.info@gmail.com';
					show(node);
				} else if (output.grade == false) {
					node.innerHTML = 'You do not have a grade assigned to yourself, Please contact at Runtime.Terror.info@gmail.com';
					console.log(node);
					show(node);
				} else if (output.signature == false) {
					node.innerHTML = 'Your do not have uploaded your signatures yet, please complete your profile.';
					show(node);
				} else {
					location.href = '/Write-Application';
				}
			}
		};
		http.open('POST', url, true);
		http.setRequestHeader('Content-Type', 'Application/json');
		http.send(JSON.stringify(data));
	};

	//redirect to dashboard
	app_title.onclick = function () {
		dashboard();
	};
	app_bg.onclick = function () {
		dashboard();
	};
	nav_db.children[0].onclick = function () {
		dashboard();
	};

	var chart_rec = $$('.box-1');
	for (var i = 0; i < chart_rec.length; i++) {
		chart_rec[i].onmouseover = function () {
			show(this.children[1]);
		};
		chart_rec[i].onmouseout = function () {
			hide(this.children[1]);
		};
		var check = 0;
		chart_rec[i].onclick = function () {
			if (check === 0) {
				show(this.children[1]);
				check = 1;
			} else {
				hide(this.children[1]);
				check = 0;
			}
		};
	}

	app_title.setAttribute('title', 'Create PoLL ');
	app_bg.setAttribute('title', 'Our work is more aesthetic than this!');
	nav_db.children[0].setAttribute('title', 'The User Dashboard');
	profile_btn.setAttribute('title', 'The User Profile');
	logout_btn.setAttribute('title', 'Logout?');
	add_btn.setAttribute('title', 'Write a new application?');

	var notification = $$('.notification'),
		notify_icon1 = $('.notify-icon'),
		notify_icon2 = $('.notifications-num'),
		response = $$('.notify-response'),
		mark_read = $$('.notifications-read'),
		error = $('#no-notification'),
		nt_id = $$('.notify-id'),
		read = false;

	//number of notifications
	var num = notification.length;

	//if there are no new notifications
	if (num === 0) {
		show(error);
	}

	//Assign number of notifications to the respective spots
	notify_icon1.innerHTML = num;
	notify_icon2.innerHTML = num;

	//mark as read functionality
	for (i = 0; i < num; i++) {

		// if (response[i].innerHTML === 'was accepted') {
		// 	response[i].style.color = 'green';
		// }

		mark_read[i].onclick = function () {

			read = true;
			this.parentElement.remove();
			num = --num;
			notify_icon1.innerHTML = num;
			notify_icon2.innerHTML = num;
			if (num === 0) {
				show(error);
			}

			data = {
				read: read,
				id: this.parentElement.children[3].innerHTML
			}

			//url of target file
			url = '/User-Notification/';

			//ajax call for deleting the notification from database
			var http = new XMLHttpRequest();
			http.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					var output = JSON.parse(this.responseText);
					if (output.check === true) {
						console.log('Notification Read Successfully');
					}
				}
			};
			http.open('POST', url, true);
			http.setRequestHeader('Content-Type', 'Application/json');
			http.send(JSON.stringify(data));
		};
	}

	//when notifications tab is clicked upon
	nav_db.children[1].onclick = function () {
		show($('#notification-wrapper'));

		$('#notifications-header').children[1].onclick = function () {
			hide($('#notification-wrapper'));
		};
	};


});
