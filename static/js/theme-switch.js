window.addEventListener('load', function () {

	var btn = document.getElementById('theme'),
		check = 0,
		root = document.documentElement;
	red = getComputedStyle(root)
		.getPropertyValue('--red'),
		orange = getComputedStyle(root)
		.getPropertyValue('--orange'),
		yellow = getComputedStyle(root)
		.getPropertyValue('--yellow'),
		gray = getComputedStyle(root)
		.getPropertyValue('--gray'),
		white = getComputedStyle(root)
		.getPropertyValue('--white');

	btn.onclick = function () {
		if (check == 0) {
			root.style.setProperty('--red', 'black');
			root.style.setProperty('--yellow', '#646464');
			root.style.setProperty('--orange', white);
			root.style.setProperty('--gray', red);
			check = 1;
		} else {
			root.style.setProperty('--red', red);
			root.style.setProperty('--yellow', yellow);
			root.style.setProperty('--orange', orange);
			root.style.setProperty('--gray', gray);
			check = 0;
		}
		console.log(document.cookie);
	}
});
