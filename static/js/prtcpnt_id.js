//place target url in url string
//participent id is POSTED either when the browser or tab is closed or the window is refreshed, or when the END ROOM button is clicked

window.addEventListener('load', () => {

	const url = '',
		data = {
			'participent_id': document.getElementById('participent_id').innerHTML
		},
		http = new XMLHttpRequest();


	function end_room() {
		//this is the response of AJAX request
		http.onreadystatechange = () => {
			if (this.readyState == 4 && this.status == 200) {
				console.log('BYE, have a nice day!');
				//in case of redirection, use this
				//location.href = '';
			}
		}

		//method of request
		http.open('POST', url, true);
		http.setRequestHeader('Content-type', 'Application/json');
		http.send(JSON.stringify(data));
	}


	window.onbeforeunload = event => {
		event.preventDefault();
		event.returnValue = '';
		end_room();
	}

});
